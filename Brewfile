tap "homebrew/bundle"
tap "homebrew/cask"
tap "homebrew/cask-fonts"
tap "homebrew/cask-versions"
tap "homebrew/core"
tap "homebrew/services"
# Mozilla CA certificate store
brew "ca-certificates"
# Library for manipulating PNG images
brew "libpng"
# Software library to render fonts
brew "freetype"
# XML-based font configuration API for X Windows
brew "fontconfig"
# GNU internationalization (i18n) and localization (l10n) library
brew "gettext"
# Library for decimal floating point arithmetic
brew "mpdecimal"
# Cryptography and SSL/TLS Toolkit
brew "openssl@3"
# Perl compatible regular expressions library with a new API
brew "pcre2"
# Library for command-line editing
brew "readline"
# General-purpose data compression with high compression ratio
brew "xz"
# Core application library for C
brew "glib"
# X.Org: Protocol Headers
brew "xorgproto"
# X.Org: A Sample Authorization Protocol for X
brew "libxau"
# X.Org: X Display Manager Control Protocol library
brew "libxdmcp"
# X.Org: Interface to the X Window System protocol
brew "libxcb"
# X.Org: Core X11 protocol client library
brew "libx11"
# X.Org: Library for common extensions to the X11 protocol
brew "libxext"
# X.Org: Library for the Render Extension to the X11 protocol
brew "libxrender"
# Real-time data compression library
brew "lzo"
# Low-level library for pixel manipulation
brew "pixman"
# Vector graphics library with cross-device output support
brew "cairo"
# Library and utilities for processing GIFs
brew "giflib"
# Smart font renderer for non-Roman scripts
brew "graphite2"
# OpenType text shaping engine
brew "harfbuzz"
# C/C++ and Java libraries for Unicode and globalization
brew "icu4c"
# JPEG image codec that aids compression and decompression
brew "jpeg-turbo"
# Extremely Fast Compression algorithm
brew "lz4"
# Zstandard is a real-time compression algorithm
brew "zstd"
# TIFF library and utilities
brew "libtiff"
# Color management engine supporting ICC profiles
brew "little-cms2"
# ANother Tool for Language Recognition
brew "antlr"
# Generic-purpose lossless compression algorithm by Google
brew "brotli"
# Performance-portable, length-agnostic SIMD with runtime dispatch
brew "highway"
# Library of 2D and 3D vector, matrix, and math operations
brew "imath"
# High dynamic-range image file format
brew "openexr"
# Image format providing lossless and lossy compression for web images
brew "webp"
# New file format for still image compression
brew "jpeg-xl"
# Perceptual video quality assessment based on multi-method fusion
brew "libvmaf"
# Codec library for encoding and decoding AV1 video streams
brew "aom"
# Macro processing language
brew "m4"
# Automatic configure script builder
brew "autoconf"
# Bourne-Again SHell, a UNIX command interpreter
brew "bash"
# Garbage collector for C and C++
brew "bdw-gc"
# Asynchronous DNS library
brew "c-ares"
# GNU multiple precision arithmetic library
brew "gmp"
# GNU File, Shell, and Text utilities
brew "coreutils"
# HTTP/2 C Library
brew "libnghttp2"
# C library implementing the SSH2 protocol
brew "libssh2"
# Tool for downloading RTMP streaming media
brew "rtmpdump"
# Get a file from an HTTP, HTTPS or FTP server
brew "curl"
# Clean Docker containers, images, networks, and volumes
brew "docker-clean"
# Generic library support script
brew "libtool"
# ODBC 3 connectivity for UNIX
brew "unixodbc"
# Libraries to talk to Microsoft SQL Server and Sybase databases
brew "freetds"
# Implementation of the Unicode BiDi algorithm
brew "fribidi"
# Command-line fuzzy finder written in Go
brew "fzf"
# Library for encoding and decoding .avif files
brew "libavif"
# Graphics library to dynamically manipulate images
brew "gd"
# GNU database manager
brew "gdbm"
# Toolkit for image loading and pixel buffer manipulation
brew "gdk-pixbuf"
# GitHub command-line tool
brew "gh"
# Distributed revision control system
brew "git"
# GNU implementation of the famous stream editor
brew "gnu-sed"
# Asynchronous event library
brew "libevent"
# C string library for manipulating Unicode strings
brew "libunistring"
# International domain name library (IDNA2008, Punycode and TR46)
brew "libidn2"
# ASN.1 structure parser library
brew "libtasn1"
# Low-level cryptographic library
brew "nettle"
# Library to load and enumerate PKCS#11 modules
brew "p11-kit"
# Validating, recursive, caching DNS resolver
brew "unbound"
# GNU Transport Layer Security (TLS) Library
brew "gnutls"
# Open source programming language to build simple/reliable/efficient software
brew "go"
# Manage compile and link flags for libraries
brew "pkg-config"
# Generate introspection data for GObject libraries
brew "gobject-introspection"
# Library for manipulating JPEG-2000 images
brew "jasper"
# Image manipulation
brew "netpbm"
# GNU triangulated surface library
brew "gts"
# Framework for layout and rendering of i18n text
brew "pango"
# Library to render SVG files using Cairo
brew "librsvg"
# Graph visualization software from AT&T and Bell Labs
brew "graphviz"
# GNU Ubiquitous Intelligent Language for Extensions
brew "guile"
# Kubernetes package manager
brew "helm"
# Text-based UI library
brew "ncurses"
# Improved top (interactive process viewer)
brew "htop"
# User-friendly cURL replacement (command-line HTTP client)
brew "httpie"
# Toolkit for embedding hypervisor capabilities in your application
brew "hyperkit"
# C library for encoding, decoding, and manipulating JSON
brew "jansson"
# Image manipulation library
brew "jpeg"
# Regular expressions library
brew "oniguruma"
# Lightweight and flexible command-line JSON processor
brew "jq"
# Cryptography and SSL/TLS Toolkit
brew "openssl@1.1"
# Interpreted, interactive, object-oriented programming language
brew "python@3.11"
# Implementation of JSON Schema for Python
brew "jsonschema"
# Network authentication protocol
brew "krb5"
# Kubernetes command-line interface
brew "kubernetes-cli", link: false
# Portable Foreign Function Interface library
brew "libffi"
# Implementation of the file(1) command
brew "libmagic"
# X.Org: pthread-stubs.pc
brew "libpthread-stubs"
# Data compression library
brew "lzlib"
# Apache Kafka C/C++ library
brew "librdkafka"
# Multi-platform support library with a focus on asynchronous I/O
brew "libuv"
# Just-In-Time Compiler (JIT) for the Lua programming language
brew "luajit"
# OpenResty's Branch of LuaJIT 2
brew "luajit-openresty"
# Mac App Store command-line interface
brew "mas"
# Modern programming language in the Lisp/Scheme family
brew "minimal-racket"
# Utility for managing network connections
brew "netcat"
# Platform built on V8 to build network applications
brew "node"
# Development kit for the Java programming language
brew "openjdk@11"
# Open source suite of directory software
brew "openldap"
# Perl compatible regular expressions library
brew "pcre"
# CLI for Postgres with auto-completion and syntax highlighting
brew "pgcli"
# Execute binaries from Python packages in isolated environments
brew "pipx"
# Object-relational database system
brew "postgresql@14"
# Python version management
brew "pyenv"
# Interpreted, interactive, object-oriented programming language
brew "python@3.10"
# Interpreted, interactive, object-oriented programming language
brew "python@3.9"
# Safe, concurrent, practical language
brew "rust"
# Python 2 and 3 compatibility utilities
brew "six"
# User interface to the TELNET protocol
brew "telnet"
# Official documentation format of the GNU project
brew "texinfo"
# Code-search similar to ack
brew "the_silver_searcher"
# Lightweight BitTorrent client
brew "transmission-cli"
# Extraction utility for .zip compressed archives
brew "unzip"
# Internet file retriever
brew "wget"
# HTTP benchmarking tool
brew "wrk"
# Process YAML, JSON, XML, CSV and properties documents from the CLI
brew "yq"
# Tracks most-used directories to make cd smarter
brew "z"
# Next-generation plugin manager for zsh
brew "zplug"
# UNIX shell (command interpreter)
brew "zsh"
# Memory training application
cask "anki"
# Desktop password and login vault
cask "bitwarden"
# Universal database tool and SQL client
cask "dbeaver-community"
# Online diagram software
cask "drawio"
# Web browser
cask "firefox"
cask "font-fira-code"
# Web browser
cask "google-chrome"
# Free and open-source media player
cask "iina"
# Terminal emulator as alternative to Apple's Terminal app
cask "iterm2"
# Clipboard manager
cask "maccy"
# Open-source, self-hosted Slack-alternative
cask "mattermost"
# Knowledge base that works on top of a local folder of plain text Markdown files
cask "obsidian"
# Collaboration platform for API development
cask "postman"
# Music streaming service
cask "spotify"
# Messaging app with a focus on speed and security
cask "telegram"
# Unpacks archive files
cask "the-unarchiver"
# To-do list
cask "todoist"
# Open-source BitTorrent client
cask "transmission"
# Open-source code editor
cask "visual-studio-code"
# Multimedia player
cask "vlc"
# Web browser
cask "yandex"
# Cloud storage
cask "yandex-disk"
# Tune in to Yandex Music and get personal recommendations
cask "yandex-music"
# Video communication and virtual meeting platform
cask "zoom"
mas "Ebook Converter", id: 1081457679
mas "GarageBand", id: 682658836
mas "hide.me VPN", id: 953040671
mas "iMovie", id: 408981434
mas "Keynote", id: 409183694
mas "Kindle Classic", id: 405399194
mas "Numbers", id: 409203825
mas "Pages", id: 409201541
mas "Pocket", id: 568494494
mas "Save to Pocket", id: 1477385213
vscode "42crunch.vscode-openapi"
vscode "alexdima.copy-relative-path"
vscode "casualjim.gotemplate"
vscode "charliermarsh.ruff"
vscode "chintans98.markdown-jira"
vscode "eamodio.gitlens"
vscode "editorconfig.editorconfig"
vscode "evzen-wybitul.magic-racket"
vscode "gitlab.gitlab-workflow"
vscode "golang.go"
vscode "mikestead.dotenv"
vscode "ms-azuretools.vscode-docker"
vscode "ms-ceintl.vscode-language-pack-ru"
vscode "ms-python.debugpy"
vscode "ms-python.python"
vscode "ms-python.vscode-pylance"
vscode "redhat.vscode-yaml"
vscode "renesaarsoo.sql-formatter-vsc"
vscode "teabyii.ayu"
vscode "yzhang.markdown-all-in-one"
